package com.verhas.google.analytics.servlet.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * Created by IntelliJ IDEA.
 * User: verhasi
 * Date: 1/27/12
 * Time: 11:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class GAFilter implements Filter {
    private FilterConfig filterConfig;
    private String gaSnippet;

    /**
     * Called by the web container to indicate to a filter that it is being placed into
     * service. The servlet container calls the init method exactly once after instantiating the
     * filter. The init method must complete successfully before the filter is asked to do any
     * filtering work. <br><br>
     * <p/>
     * The web container cannot place the filter into service if the init method either<br>
     * 1.Throws a ServletException <br>
     * 2.Does not return within a time period defined by the web container
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        gaSnippet = "<script type=\"text/javascript\">\n" +
                "var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");\n" +
                "document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));\n" +
                "</script>\n" +
                "<script type=\"text/javascript\">\n" +
                "try{\n" +
                "var pageTracker = _gat._getTracker(\"" + filterConfig.getInitParameter("code") + "\");\n" +
                "pageTracker._trackPageview();\n" +
                "} catch(err) {}\n" +
                "</script>";
    }

    /**
     * The <code>doFilter</code> method of the Filter is called by the container
     * each time a request/response pair is passed through the chain due
     * to a client request for a resource at the end of the chain. The FilterChain passed in to this
     * method allows the Filter to pass on the request and response to the next entity in the
     * chain.<p>
     * A typical implementation of this method would follow the following pattern:- <br>
     * 1. Examine the request<br>
     * 2. Optionally wrap the request object with a custom implementation to
     * filter content or headers for input filtering <br>
     * 3. Optionally wrap the response object with a custom implementation to
     * filter content or headers for output filtering <br>
     * 4. a) <strong>Either</strong> invoke the next entity in the chain using the FilterChain object (<code>chain.doFilter()</code>), <br>
     * * 4. b) <strong>or</strong> not pass on the request/response pair to the next entity in the filter chain to block the request processing<br>
     * * 5. Directly set headers on the response after invocation of the next entity in the filter chain.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (filterConfig == null)
            return;
        if (response instanceof HttpServletResponse) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            Writer outWriter = httpServletResponse.getWriter();
            HttpServletResponseWrapper wrappedResponse = new HttpServletResponseWrapper(httpServletResponse) {
                private CharArrayWriter output;

                {
                    output = new CharArrayWriter();
                }

                @Override
                public String toString() {
                    return output.toString();
                }

                @Override
                public PrintWriter getWriter() throws IOException {
                    return new PrintWriter(output);
                }
            };
            chain.doFilter(request, wrappedResponse);
            String output = wrappedResponse.toString();
            int bodyEndPosition = output.indexOf("</body>") - 1;
            if (-1 < bodyEndPosition) {
                outWriter.write(output.substring(0, bodyEndPosition) + gaSnippet + output.substring(bodyEndPosition + 1, output.length() - 1));
            } else {
                 response.getWriter().write(output);
            }
            response.getWriter().close();
        }
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service. This
     * method is only called once all threads within the filter's doFilter method have exited or after
     * a timeout period has passed. After the web container calls this method, it will not call the
     * doFilter method again on this instance of the filter. <br><br>
     * <p/>
     * This method gives the filter an opportunity to clean up any resources that are being held (for
     * example, memory, file handles, threads) and make sure that any persistent state is synchronized
     * with the filter's current state in memory.
     */
    public void destroy() {
        this.filterConfig = null;
    }
}
